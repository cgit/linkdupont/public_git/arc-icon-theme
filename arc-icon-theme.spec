%global		debug_package	%{nil}
%global		gittag		20161122

Name:		arc-icon-theme
Version:	1.0
Release:	%{gittag}.1%{?dist}
Summary:	A flat theme with transparent elements

License:	GPLv3
URL:		https://github.com/horst3180/arc-icon-theme
Source0:	https://github.com/horst3180/arc-icon-theme/archive/%{gittag}.tar.gz#/%{name}-%{gittag}.tar.gz

Requires:	moka-icon-theme
Requires:	adwaita-icon-theme
BuildRequires:	autoconf
BuildRequires:	automake

BuildArch:	noarch


%description
%{summary}.


%prep
%autosetup -n arc-icon-theme-%{gittag}

%build
NOCONFIGURE=1 ./autogen.sh
%configure
%make_build


%install
%make_install


%files
%license COPYING
%doc README.md CREDITS
%{_datadir}/icons/Arc


%changelog
* Sat Nov 26 2016 Link Dupont <linkdupont@fedoraproject.org> - 1.0-20161122.1
- Initial package
